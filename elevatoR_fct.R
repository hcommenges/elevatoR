################################
######## elevatoR - functions
################################


# population variance ----

varp <- function(x){
  mean((x - mean(x, na.rm = TRUE))^2, na.rm = TRUE)
}


# cut profile ----

cut_profile <- function(profile, regspan = 0.05, seqspan = 10, windowspan = 5, varthres = 0.9){
  colnames(profile)[1:2] <- c("DISTANCE", "ELEVATION")
  localReg <- loess(formula = ELEVATION ~ DISTANCE, data = profile, span = regspan)
  seqX <- seq(from = 0, to = max(profile$DISTANCE), by = seqspan)
  lengthX <- length(seqX)
  localPred <-  predict(localReg, data.frame(DISTANCE = seqX))
  localSlope <- diff(localPred, lag = 1) / (1 * seqspan)
  movingVariance <- rollapply(localSlope, width = windowspan, FUN = varp, fill = NA)
  cutVariance <- quantile(movingVariance, probs = varthres, na.rm = TRUE)
  idSubsamp <- which(movingVariance > cutVariance)
  idLag <- which(diff(idSubsamp, lag = 1) > 1)
  nbPts <- c(idLag, length(idSubsamp)) - c(1, idLag + 1) + 1
  brks <- tibble(DISTANCE = seqX[2:(length(seqX)-1)][idSubsamp], 
                    VARIANCE = movingVariance[idSubsamp],
                    GROUP = rep(letters[1:length(nbPts)], nbPts)) %>% 
    group_by(GROUP) %>% 
    summarise(BRKS = DISTANCE[which.max(VARIANCE)]) %>% 
    pull(BRKS)
  brksVal <- c(0, brks, max(profile$DISTANCE))
  
  profilePlot <- ggplot(profile) + 
    geom_line(aes(DISTANCE, ELEVATION), color = "tomato4", size = 1.5) +
    xlab("Distance (m)") + ylab("Elevation (m)") +
    theme_minimal()
  
  loessPlot <- ggplot() +
    geom_point(data = profile, aes(DISTANCE, ELEVATION), color = "grey50") +
    geom_line(data = tibble(DISTANCE = seqX, ELEVATION = localPred),
              aes(DISTANCE, ELEVATION), color = "tomato4", size = 1) +
    xlab("Distance (m)") + ylab("Elevation (m)") +
    theme_minimal()
  
  slopePlot <- ggplot(data = tibble(DISTANCE = seqX[2:(length(seqX))], SLOPE = localSlope),
                         aes(DISTANCE, SLOPE)) +
    geom_line(color = "grey40") +
    geom_point(color = "grey40", size = 0.8) +
    xlab("Distance (m)") + ylab("Local slope") +
    theme_minimal()
  
  variancePlot <- ggplot(data = tibble(DISTANCE = seqX[2:(length(seqX))], VARIANCE = movingVariance),
                         aes(DISTANCE, VARIANCE)) +
    geom_line(color = "grey40") +
    geom_point(color = "grey40", size = 0.8) +
    geom_hline(yintercept = cutVariance, color = "firebrick") + 
    geom_vline(xintercept = brksVal, color = "firebrick", linetype = 2) + 
    xlab("Distance (m)") + ylab("Moving variance") +
    theme_minimal()
  
  profilePlot <- ggplot(profile) + 
    geom_line(aes(DISTANCE, ELEVATION), color = "tomato4", size = 1.5) +
    geom_vline(xintercept = brksVal, color = "grey30", linetype = 2) + 
    xlab("Distance (m)") + ylab("Elevation (m)") +
    theme_minimal()
  
  gridExtra::grid.arrange(loessPlot, slopePlot, variancePlot, profilePlot)
  return(brksVal)
}


# plot profile and slope ----

draw_profile <- function(profile, brks){
  colnames(profile)[1:2] <- c("DISTANCE", "ELEVATION")
  brksAlt <- profile$ELEVATION[sapply(brks, function(x) which.min(abs(profile$DISTANCE - x)))]
  difAlt <- diff(brksAlt, lag = 1)
  difDist <- diff(brks, lag = 1)
  pctSlope <- 100 * difAlt / difDist
  
  tabSegment <- tibble(x = brks[1:length(brks)-1],
                       xend = brks[2:length(brks)],
                       y = min(profile$ELEVATION) - 30,
                       yend = min(profile$ELEVATION) - 30,
                       color = ifelse(pctSlope > 0, "n", "p"),
                       text = paste(round(pctSlope), "%")
  ) %>% 
    mutate(xtext = (x + xend) / 2)
  
  ggplot() +
    geom_line(data = profile, aes(x = DISTANCE, y = ELEVATION), color = "grey20", size = 2) +
    geom_segment(data = tabSegment,
                 aes(x = x, xend = xend, y = y, yend = yend, color = color), size = 6) +
    geom_text(data = tabSegment, aes(x = xtext, y = y, label = text), color = "white") +
    geom_vline(xintercept = brks, color = "grey20", linetype = 2) +
    scale_color_manual(values = c("firebrick", "navyblue")) +
    scale_x_continuous("Distance (m)", breaks = round(brks)) +
    scale_y_continuous("Altitude (m)") +
    theme_minimal() + theme(legend.position="none")
}
